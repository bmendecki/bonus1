from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views
from django.views.generic import RedirectView

urlpatterns = [

    url(r'^login/$', auth_views.login, name='login'),
    url(r'^$', auth_views.login, name='login'),
    url(r'logout/', auth_views.logout, {'next_page': 'login'}, name='logout'),
    url(r'^user/$', views.index, name='user'),
    url(r'^admin/$', views.admin, name='admin'),
    url(r'^wniosek/$', views.wniosek, name='wniosek'),
    url(r'^thanks/$', views.thanks, name='thanks'),
    url(r'^month_reach_bonus/4324ffsfs42$', views.send_update_request),
    url(r'^(?P<hash>[a-z0-9]+)/(?P<user_id>[0-9]+)/(?P<days>[0-9]+)/(?P<days_bonus>((\d+-\d+-\d+),?\+?)+)/$', views.update_bonus, name='update'),
    url(r'^(?P<hash>[a-z0-9]+)/update_all/(?P<status>[0-1])$', views.update_all, name='update_all'),
    url(r'^modkal/$', views.pickuser, name='pickuser'),
    url(r'^modkal/(?P<client_id>[0-9]+)/(?P<admin_id>[0-9]+)/$', views.modifycalendar, name='modifycalendar'),
    url(r'^resetpasswordconfirm/(?P<uidb64>[0-9A-Za-z]+)/$', views.reset_password_confirm, name='reset_password_confirm'),
    url(r'^reset_password', views.reset_password, name='reset_password'),
    url(r'^createuser/$', views.create_user, name='create_user'),
    url(r'^resetdays/czego1szukasz2ancymonie3barbarzynco4$', views.reset_days, name='reset_days'),
    url(r'^backup/$', views.backup, name='backup'),
    url(r'^sprawdz/$', views.checkcallendar, name='checkcallendar'),
    url(r'^forceb/(?P<user_id>[0-9]+)/(?P<days>[0-9]+)/$', views.force_bonus, name='force')

]