# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Bonus(models.Model):
    user = models.OneToOneField(User)
    bonus = models.FloatField(max_length=2)

    def __str__(self):
        return str(self.bonus)


class Hash(models.Model):
    user = models.OneToOneField(User)
    hash = models.CharField(max_length=15)

    def __str__(self):
        return str(self.hash)


class Manager(models.Model):
    user = models.OneToOneField(User)
    email = models.EmailField(max_length=100)

    def __str__(self):
        return str(self.email)


class BonusDays(models.Model):
    user = models.OneToOneField(User)
    date_bonus = models.TextField()

    def __str__(self):
        return str(self.date_bonus)


class Pending(models.Model):
    user = models.OneToOneField(User)
    dates = models.TextField()
    # status 1 - aktywny do kliknięcia
    status = models.IntegerField()
    url = models.TextField()

    def __str__(self):
        return str(self.dates) + str(self.status) + str(self.url)


class BonusMonths(models.Model):
    year = models.IntegerField()
    month = models.IntegerField()
    # status 0 - oczekujący, 1 - wyrobiony, 2 - niewyrobiony
    status = models.IntegerField()
    url = models.TextField()
    hash = models.TextField(default='')

    def __str__(self):
        return str(self.year) + str(self.month) + str(self.status) + str(self.url) + str(self.hash)




