# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-02-14 10:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_view', '0009_auto_20170124_1121'),
        ('user_view', '0005_bonusdays_bonusmonths_manager_pending'),
    ]

    operations = [
    ]
