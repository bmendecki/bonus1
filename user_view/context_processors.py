from django.conf import settings

murl = 'http://webdev-dzp.infr.co:8000/bonus/'
staticurl = 'http://0.0.0.0:8000/static/'

def main_url(request):
    global murl
    global staticurl
    return {'MAIN_URL':  murl, 'STATICURL': staticurl}
