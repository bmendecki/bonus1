from __future__ import unicode_literals

from django.apps import AppConfig


class UserViewConfig(AppConfig):
    name = 'user_view'
