# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User


MONTHS_TUPLE = (
    (1, "Styczeń"),
    (2, "Luty"),
    (3, "Marzec"),
    (4, "Kwiecień"),
    (5, "Maj"),
    (6, "Czerwiec"),
    (7, "Lipiec"),
    (8, "Sierpień"),
    (9, "Wrzesień"),
    (10, "Październik"),
    (11, "Listopad"),
    (12, "Grudzień")
)
YEAR_TUPLE = {
    (2020, "2020"),
    (2019, "2019"),
    (2018, "2018"),
    (2017, "2017")
}


class ContactForm(forms.Form):
    days_quant = forms.IntegerField(label=u'Ilość dni')
    days_bonus = forms.CharField(widget=forms.Textarea, label=u'Dni wzięte')


class ReportForm(forms.Form):
    month = forms.ChoiceField(
        required=True,
        label=u'Miesiąc do raportu',
        choices=MONTHS_TUPLE
    )
    year = forms.ChoiceField(
        required=True,
        label=u'Rok do raportu',
        choices=YEAR_TUPLE
    )

class PickUserForm(forms.Form):
    client = forms.ModelChoiceField(
        queryset=User.objects.all(),
        empty_label=None,
        label='Użytkownik'
    )


class ModifyUserForm(forms.Form):
    user = forms.IntegerField(label=u'User ID')
    username = forms.CharField(label=u'Imię')
    surname = forms.CharField(label=u'Nazwisko')
    days_quant = forms.IntegerField(label=u'Ilość dni')
    days_bonus = forms.CharField(widget=forms.Textarea, label=u'Dni wzięte')


class PasswordResetRequestForm(forms.Form):
    email = forms.CharField(label="Adres E-mail", max_length=254)


class PasswordResetConfirmationForm(forms.Form):
    email = forms.CharField(label="Adres E-mail", max_length=254)
    new_passsword = forms.CharField(label="Nowe hasło", max_length=254, widget=forms.PasswordInput())
    new_passsword_re = forms.CharField(label="Powrórz nowe hasło", max_length=254, widget=forms.PasswordInput())


class NewUserForm(forms.Form):
    username = forms.CharField(label="Nazwa")
    email = forms.CharField(label="Email")
    first_name = forms.CharField(label="Imię")
    last_name = forms.CharField(label="Nazwisko")
    manager_email = forms.CharField(label="Email managera")
