document.getElementById('id_user').value = document.querySelector('code').getAttribute('data-clientid');
document.getElementById('id_username').value = document.querySelector('code').getAttribute('data-clientfirstname');
document.getElementById('id_surname').value = document.querySelector('code').getAttribute('data-clientlastname');
document.getElementById('id_days_quant').value = 0;
document.getElementById('id_days_bonus').value = '';

$(document).ready(function(){
    var todd = $('[data-d]').data('d');
    var todm = $('[data-d]').data('m');

function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}

function removeElement(el, arr){
    for(var i = arr.length; i--;){
        if(arr[i] == el){
            arr.splice(i,1);
        }
    }
    return arr;
}

function getMonth(el){
    var mname = el.parentElement.parentElement.children[0].children[0];
    all_m = document.querySelectorAll('.month');
    for(var i = 0; i<all_m.length ;i++){
        var m = '';
        if(all_m[i].innerText == mname.innerText){
            return (1+i)/2;
        }
    }
}
function getDay(el){
    if(Number.isInteger(parseInt(el.innerText))){
        return parseInt(el.innerText);
    }
    return 0;
}

function getDate(el){
    return getDay(el)+"-"+getMonth(el)+"-"+2017;
}

var bd = [];
var ddd = 0;

function goodDay(el){
    if(hasClass(el, 'sat') || hasClass(el, 'sun')){
        return false;
    }
    if(!(getDay(el) > 0)){
        return false;
    }
    if(hasClass(el, 'swi') || hasClass(el, 'byl')){
        return false;
    }
    return true;
}

function monthNumber(el){
    s = el.parentNode.parentNode.children[0].innerText;
    for(var i = 0; i <= document.querySelectorAll('th.month').length;i++){
        if(document.querySelectorAll('th.month')[i].innerText == s){
            return i+1;
        }
    }
}

function swietuj(arr){
    for(var i = 0; i < arr.length; i++){ //dla kazdego swieta
        var d = arr[i].split("-");
        m = $('th.month')[d[1]-1];
        day = '';
        trs = m.parentNode.parentNode.children
        for(var j = 0; j < trs.length; j++){ // dla kazdego tr z miesiaca
            for(var k = 0; k < trs[j].children.length; k++){ //dla kazdego td
                if(parseInt(trs[j].children[k].innerText) == d[0]){
                    addClass(trs[j].children[k], 'swi');
                }
            }
        }
    }
    if(uzyte_dni.length > 0){
        for(var i = 0; i < uzyte_dni.length; i++){ //dla kazdego swieta
            var d = uzyte_dni[i].split("-");
            m = $('th.month')[d[1]-1];
            day = '';
            trs = m.parentNode.parentNode.children
            for(var j = 0; j < trs.length; j++){ // dla kazdego tr z miesiaca
                for(var k = 0; k < trs[j].children.length; k++){ //dla kazdego td
                    if(parseInt(trs[j].children[k].innerText) == d[0]){
                        addClass(trs[j].children[k], 'used');
                    }
                }
            }
        }
    }
    /*days = document.querySelectorAll('td > table > tbody > tr > td');
    for(var i = 0; i < days.length; i++){ // dla kazdego dnia
        if(Number.isInteger(parseInt(days[i].innerText)) &&
            parseInt(days[i].innerText) <= todd){
                if(monthNumber(days[i]) <= todm){
                    addClass(days[i], 'byl');
                }
        }
    }*/
}

swietuj(swieta);
function chout(e){
    //console.log(e);
    el = document.elementFromPoint(e.clientX, e.clientY);
    //console.log(el);
    if(goodDay(el)){
        $('#ddw span').empty();
        if(hasClass(el, 'checked')){
            ddd--;
            removeElement(getDate(el), bd);
            removeClass(el, 'checked');
        } else {
            if(ddd+1 > parseFloat(document.querySelector('code').getAttribute('data-d'))){
            }else{
                ddd++;
                bd.push(getDate(el));
                addClass(el, 'checked');
            }
        }
        document.querySelector('#id_days_quant').value = ddd;
        document.querySelector('#id_days_bonus').value = JSON.stringify(bd);
        var s = '';
        for(var i = 0; i < bd.length; i++){
            var el = document.createElement('span');
            var node = document.createTextNode(bd[i]);
            el.appendChild(node);
            document.querySelector('#ddw span').appendChild(el);
        }
        document.querySelector('#ddw u').innerText = ddd;

    }
}
for(var i = 0; i < document.querySelectorAll('.calendar-form > table > tbody > tr > td > table > tbody > tr > td').length; i++){
    var elem = document.querySelectorAll('.calendar-form > table > tbody > tr > td > table > tbody > tr > td')[i];
        elem.addEventListener('click', chout, true);
    }
})
