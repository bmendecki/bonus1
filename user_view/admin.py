from django.contrib import admin
from .models import Bonus, Manager, BonusDays, Pending,BonusMonths

admin.site.register(Bonus)
admin.site.register(Manager)
admin.site.register(BonusDays)
admin.site.register(Pending)
admin.site.register(BonusMonths)

