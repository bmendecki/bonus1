# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models.query_utils import Q
from django.contrib.auth.models import User
from django.core.mail import send_mail, BadHeaderError, EmailMultiAlternatives
from django.http import HttpResponse
from .forms import ContactForm, ReportForm, PickUserForm, ModifyUserForm, PasswordResetRequestForm,\
    PasswordResetConfirmationForm, NewUserForm
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from .models import Bonus, Hash, BonusDays, Pending, Manager, BonusMonths
import os
import binascii
from django.template.loader import render_to_string
import calendar
import datetime
import simplejson as json
import xlwt
import codecs
import sys
from context_processors import main_url, murl
import re

swieta = [u"1-1-2017", u"6-1-2017", u"16-4-2017", u"17-4-2017", u"1-5-2017", u"3-5-2017", u"4-6-2017"]

def index(request):
    all_managers = []
    user_is_manager = 0
    for user in User.objects.all():
        try:
            all_managers.append(user.manager.email)
        except AttributeError:
            redirect('admin')


    if request.user.email in all_managers:
        user_is_manager = 1

    return render(request, 'user_view/index.html', {
        'all_users': User.objects.all(),
        'user_is_manager': user_is_manager
    })


def admin(request):
    UTF8Writer = codecs.getwriter('utf8')
    sys.stdout = UTF8Writer(sys.stdout)
    all_users = User.objects.all()
    all_pending = Pending.objects.all()
    if request.method == 'POST':
        form = ReportForm(request.POST)
        if form.is_valid():

            def is_something(days, month, year):
                for day in days:
                    if u''+month+'-'+year in day:
                        return True
                return False
            def is_this(day, month, year):
                if u''+month+'-'+year in day:
                    return True
                return False

            raport = xlwt.Workbook(encoding="utf-8")
            mon = form.cleaned_data['month']
            yea = form.cleaned_data['year']
            sheet1 = raport.add_sheet(mon + ' ' + yea)
            style = xlwt.easyxf('font: name Arial, color-index black')
            style_date = xlwt.easyxf(num_format_str='DD-MM-YYYY')
            sheet1.write(0, 0, 'data wygenerowania', style)
            sheet1.write(0, 1, datetime.datetime.today(), style_date)
            sheet1.write(1, 0, 'imię', style)
            sheet1.write(1, 1, 'nazwisko', style)
            sheet1.write(1, 2, 'daty', style)
            l = len(all_users)
            i = 0
            j = 0
            sheet1.write(0, 4, 'year:' + str(yea), style)
            sheet1.write(0, 5, 'month:' + str(mon), style)
            for user in all_users:
                client_days = user.bonusdays.date_bonus.split(',')
                if is_something(client_days, mon, yea):
                    client_name = user.first_name.encode("utf-8")
                    clinet_last_name = user.last_name.encode("utf-8")

                    sheet1.write(2+i, 0, str(client_name), style)
                    sheet1.write(2+i, 1, str(clinet_last_name), style)
                    for day in client_days:
                        if is_this(day, mon, yea):
                            sheet1.write(2+i, 2+j, day, style_date)
                            j += 1
                        else:
                            pass
                    j = 0
                    i += 1

            data = str(datetime.datetime.today())
            if not os.path.exists('raporty'):
                os.makedirs('raporty')
            raport.save('raporty/raport '+data+'.xls')
            with open('raporty/raport '+data+'.xls') as file:
                response = HttpResponse(file.read(), content_type='application/vnd.ms-excel')
                response['Content-Disposition'] = 'attachement; filename="raport '+data+'.xls"'
            return response

    form = ReportForm()
    return render(request, 'user_view/admin.html',
                  {
                      'all_users': all_users,
                      'form': form,
                      'year': datetime.datetime.today().year,
                      'all_pending': all_pending
                  })


def wniosek(request):
    global murl
    cal = calendar.HTMLCalendar()
    cal = cal.formatyear(2017, width=4)

    td = datetime.datetime.today()  # dzisiejsza data, info dla js

    global swieta
    swieta = swieta

    wziete_db_q = BonusDays.objects.all().filter(user=User.objects.all().filter(pk=request.user.id))
    podzielone = wziete_db_q[0].date_bonus if wziete_db_q[0].date_bonus != "brak" else '0-1-2017'
    if podzielone[0] == ',':
        podzielone = podzielone[1:]

    podzielone = podzielone.split(',')
    uzyte = podzielone

    nie_do_uzycia = swieta + podzielone
    podzielone = json.dumps(podzielone)
    nie_do_uzycia = json.dumps(nie_do_uzycia)#json.dumps(swieta + podzielone)

    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        days_to_spend = Bonus.objects.all().filter(user=User.objects.all()
                                                   .filter(pk=request.user.id)).first().bonus


        if form.is_valid():

            token = binascii.b2a_hex(os.urandom(15))
            h = get_object_or_404(Hash, user=request.user)
            h.hash = token
            h.save()

            def parsuj(el):
                splited = el.split('-')
                els = [i.strip('0') for i in splited]
                el = els[0] + '-' + els[1] + '-' + els[2]
                return el

            #  walidacja wzietych dni
            planowane_do_wziecia = json.loads(form.cleaned_data['days_bonus'])
            planowane_do_wziecia = [parsuj(i) for i in planowane_do_wziecia]
            if len(planowane_do_wziecia) == 0:
                return HttpResponse('Wysłano pusty formularz.')
            if len(planowane_do_wziecia) > days_to_spend:
                return HttpResponse('Nie wysłano formularza')
            # te same z nie_do_uzycia i planowane_do_wziecia
            dp = [i for i in nie_do_uzycia if i in planowane_do_wziecia]

            if len(dp) > 0:
                return HttpResponse('<h1>Nie cwaniakuj</h1><br><h4>uzytkownik zostal zgloszony</h4>')

            planowane_do_wziecia = json.dumps(planowane_do_wziecia)

            planowane_do_wziecia = planowane_do_wziecia.replace('"', '').replace("'", "").replace('[', '')\
                .replace(']', '').replace('+', '').replace(' ','')

            # koniec walidacji dni wolnych

            days_quant = form.cleaned_data['days_quant']
            days_bonus = form.cleaned_data['days_bonus']
            days = str(days_quant)
            pretty_days = re.sub('[\[\]\"]', '', days_bonus)



            url = murl + token + '/' + str(
                request.user.id) + '/' + days + '/' + planowane_do_wziecia
            html_message = render_to_string('user_view/wniosek-mail.html',
                                            {'days': days, 'url': url, 'days_bonus': pretty_days})

            try:
                send_mail('wniosek bonusowy', 'info', request.user.email, [request.user.manager.email],
                          html_message=html_message)
            except BadHeaderError:
                return HttpResponse('Invalid header found ')
            # tutaj wniosek wpada do Pending ze statusem 1
            p = get_object_or_404(Pending, user=request.user)
            p.dates = days_bonus
            p.status = 1
            p.url = url
            p.save()
            print('zapisano')

            return redirect(
                'thanks')  # , {'days_to_spend':days_to_spend, 'dp':dp, 'planowane_do_wziecia':planowane_do_wziecia})
    return render(request, 'user_view/wniosek.html', {'form': form, 'cal': cal, 'td': td, 'ndu': nie_do_uzycia,
                                                      'used':podzielone, 'daysused':uzyte})


def pickuser(request):
    if request.method == 'GET':
        form = PickUserForm()
    else:
        form = PickUserForm(request.POST)
        if form.is_valid():
            form_user = form.cleaned_data['client']
            return redirect('../modkal/'+str(form_user.id)+'/'+str(request.user.id))
        return HttpResponse('Walidacja nie została spełniona')
    return render(request, 'user_view/pickuser.html', {'form': form})


def modifycalendar(request, client_id, admin_id):
    global murl
    cal = calendar.HTMLCalendar()
    cal = cal.formatyear(2017, width=4)

    td = datetime.datetime.today()  # dzisiejsza data, info dla js

    global swieta
    swieta = swieta

    wziete_db_q = BonusDays.objects.all().filter(user=User.objects.all().filter(pk=client_id))
    podzielone = wziete_db_q[0].date_bonus if wziete_db_q[0].date_bonus != "brak" else '0-1-2017'

    podzielone = podzielone.split(',')
    uzyte = json.dumps(podzielone)
    days_to_spend = Bonus.objects.all().filter(user=User.objects.all()
                                               .filter(pk=client_id)).first().bonus

    nie_do_uzycia = swieta + podzielone

    nie_do_uzycia = json.dumps(nie_do_uzycia)#json.dumps(swieta + podzielone)

    clinet_info= {
        'du': days_to_spend,  # dni do wydania
        'imie': User.objects.all().filter(pk=client_id)[0].first_name,
        'nazwisko': User.objects.all().filter(pk=client_id)[0].last_name,
        'dni_wydane': podzielone,
        'bonus': Bonus.objects.all().filter(user=User.objects.all()
                                            .filter(pk=client_id)).first().bonus,
        'dniuzyte': uzyte,
        'id': User.objects.all().filter(pk=client_id)[0].id,
    }

    if request.method == 'GET':
        form = ModifyUserForm()

    else:
        form = ModifyUserForm(request.POST)


        if form.is_valid():
            client_user_id = form.cleaned_data['user']
            if client_id != str(client_user_id):
                return HttpResponse('nastąpiła nieoczekiwana ingerencja w formularz')
            client_username = form.cleaned_data['username']
            client_surnmae = form.cleaned_data['surname']
            token = binascii.b2a_hex(os.urandom(15))
            h = get_object_or_404(Hash, user_id=client_user_id)
            h.hash = token
            h.save()

            def parsuj(el):
                splited = el.split('-')
                els = [i.strip('0') for i in splited]
                el = els[0] + '-' + els[1] + '-' + els[2]
                return el

            #  walidacja wzietych dni
            planowane_do_wziecia = json.loads(form.cleaned_data['days_bonus'])
            planowane_do_wziecia = [parsuj(i) for i in planowane_do_wziecia]
            # te same z nie_do_uzycia i planowane_do_wziecia
            dp = [i for i in nie_do_uzycia if i in planowane_do_wziecia]

            if len(dp) > 0:
                return HttpResponse('<h1>Dni się powtarzają</h1><br><h4>lub święto</h4>')

            if len(planowane_do_wziecia) == 0:
                return HttpResponse('Wysłano pusty formularz.')
            planowane_do_wziecia = json.dumps(planowane_do_wziecia)

            planowane_do_wziecia = planowane_do_wziecia.replace('"', '').replace("'", "").replace('[', '') \
                .replace(']', '').replace('+', '').replace(' ','')

            # koniec walidacji dni wolnych

            days_quant = form.cleaned_data['days_quant']
            days_bonus = form.cleaned_data['days_bonus']
            days = str(days_quant)

            url = murl + token + '/' + str(
                client_id) + '/' + days + '/' + planowane_do_wziecia
            html_message = render_to_string('user_view/admin-potwierdzenie.html',
                                            {'days': days, 'url': url, 'days_bonus': days_bonus,
                                             'client_name': client_username, 'client_surname': client_surnmae})
            '''
            try:
                send_mail('wniosek bonusowy', 'info', request.user.email, [request.user.manager.email],
                          html_message=html_message)
            except BadHeaderError:
                return HttpResponse('Invalid header found ')
            '''
            # tutaj wniosek wpada do Pending ze statusem 1
            p = get_object_or_404(Pending, user=client_id)
            p.dates = days_bonus
            p.status = 1
            p.url = url
            p.save()
            return HttpResponse(html_message)

    return render(request, 'user_view/modifycalendar.html', {'form': form, 'clinet_info': clinet_info, 'cal': cal,
                                                             'td': td, 'ndu': nie_do_uzycia,
                                                             'client_id': client_id, 'daysused':uzyte})


def thanks(request):
    return HttpResponse('Wniosek wysłany poprawnie i oczekuje na akceptację managera: ' + str(request.user.manager.email))


def update_bonus(request, hash, user_id, days, days_bonus):
    bonus = get_object_or_404(Bonus, user_id=user_id)
    h = get_object_or_404(Hash, user_id=user_id)
    dbo = get_object_or_404(BonusDays, user_id=user_id)
    pending = get_object_or_404(Pending, user_id=user_id)

    if hash == h.hash:
        # reset hash
        token = binascii.b2a_hex(os.urandom(15))
        h.hash = token
        h.save()
        d_t_db = days_bonus.replace('+', '')
        days_bonus = days_bonus.replace('+', '').split(',') if ',' in days_bonus else days_bonus  #json.dumps(days_bonus.replace("&quot;", '"').replace("\\", ""))
        if bonus.bonus - int(days) >= 0:
            bonus.bonus -= int(days)
            bonus.save()
            dbo.date_bonus = dbo.date_bonus.replace("brak", "") + ',' + d_t_db
            if dbo.date_bonus[0] == ',':
                dbo.date_bonus = dbo.date_bonus[1:]
            dbo.save()
            pending.status = 0
            pending.save()

        else:
            return HttpResponse('Osiągnięto limit bonusa')
        # potwierdzenie do usera
        send_mail('Zaackeptowano wniosek', 'Twój wniosek został zaakceptowany', request.user.manager.email, [request.user.email])
        return redirect('user')
    else:
        return HttpResponse('Error: hash doesnt match')


def send_update_request(request):
    global murl
    token = binascii.b2a_hex(os.urandom(15))
    url = murl + token + '/' + 'update_all/0'
    url2 = murl + token + '/' + 'update_all/1'
    # set last month
    today = datetime.date.today()
    first = today.replace(day=1)
    lastMonth = first - datetime.timedelta(days=1)
    month = lastMonth.strftime("%m")

    u = BonusMonths(year=2017, month=month, status=0, url=url, hash=token)
    u.save()
    html_message = render_to_string('user_view/update-mail.html', {'url': url, 'url2': url2, 'month': month})
    try:
        send_mail('Aktualizacja bonusa', 'info', 'no-reply', ['bartlomiej.mendecki@brandnova.com'], html_message=html_message)
    except BadHeaderError:
        return HttpResponse('Invalid header found ')
    return HttpResponse('wysłano request')


def update_all(request, hash, status):
    all_bonuss = Bonus.objects.all()
    is_hash = get_object_or_404(BonusMonths, hash=hash)
    if is_hash and status == '1':
        for b in all_bonuss:
            if b.bonus <= 24.9:  # (12+3)*1.66 = 24.9
                b.bonus += 1.66
                b.save()
        is_hash.status = status
        is_hash.save()

        return HttpResponse('Dodano bonusy!')

    if is_hash and status == '0':
        is_hash.status = status
        is_hash.save()
        return HttpResponse('Nie dodano bonusów, miesiąc zapisano ze statusem 0.')


def reset_password(request):
    global murl
    if(request.method == 'GET'):
        # oddaj fomrularz
        form = PasswordResetRequestForm()
    if(request.method == 'POST'):
        form = PasswordResetRequestForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data["email"]
            associated_users = User.objects.filter(Q(email=data))
            if associated_users.exists():
                for user in associated_users:
                    c = {
                        'email': user.email,
                        'uid': urlsafe_base64_encode((user.password)),
                        'subject': 'Reset password'
                    }

                url = murl + 'resetpasswordconfirm/' + str(c['uid']) + '/'
                html_message = render_to_string('registration/password-reset-mail.html', {'url': url})
                try:
                    send_mail('reset hasła', 'info', c['email'], [c['email']],
                          html_message=html_message)
                except BadHeaderError:
                    return HttpResponse('Invalid header found ')
                return HttpResponse('An email has been sent to ' + data + ". Please check its inbox to continue reseting password.<br>")

            '''
            spr czy istnieje email,
            wez usera
            stworz link
            wyslij maila
            return widok
            '''
            return HttpResponse('No user is associated with this email address')
    return render(request, 'registration/password.html', {'form': form})


def reset_password_confirm(request, uidb64):
    if(request.method == 'GET'):
        form = PasswordResetConfirmationForm()
        uid = urlsafe_base64_decode((uidb64))
        user = User.objects.filter(Q(password=uid))
        if user.exists():
            c = {
                'uemai' : user.first().email,
            }
            return render(request, 'registration/new-password.html', {'form': form, 'uemail': c['uemai']})
        return HttpResponse('Sorry, wrong url')
    if(request.method == 'POST'):
        form = PasswordResetConfirmationForm(request.POST)
        if form.is_valid():
            uid = urlsafe_base64_decode((uidb64))
            user = User.objects.filter(Q(password=uid))
            if user.exists():
                c = {
                    'uemai' : user.first().email,
                    'pass' : user.first().password,
                }
                if str(form.cleaned_data.get('new_passsword')) == str(form.cleaned_data.get('new_passsword_re')):
                    new_password = form.cleaned_data['new_passsword']
                    u = user.first()
                    u.set_password(new_password)
                    u.save()
                    return HttpResponse('Pomyślnie zmieniono hasło.')
                else:
                    return HttpResponse('Hasła nie są identyczne.')
        return HttpResponse('napotkano jakiś błąd, hasło nie zostało zmienione')


def create_user(request):
    if(request.method == 'GET'):
        form = NewUserForm()
    if(request.method == 'POST'):
        form = NewUserForm(request.POST)
        if form.is_valid():
            #spr czy username wolny
            #spr czy email wolny
            #spr czy manager_email istnieje
            if User.objects.all().filter(username = form.cleaned_data['username']).exists():
                return HttpResponse('Użytkownik o takiej nazwie już istnieje')
            if User.objects.all().filter(email = form.cleaned_data['email']).exists():
                return HttpResponse('Użytkownik o takim emailu już istnieje')
            if not User.objects.all().filter(email = form.cleaned_data['manager_email']).exists():
                return HttpResponse('Nie ma managera z takim emailem')
            new_user = User.objects.create_user(form.cleaned_data['username'], form.cleaned_data['email'],
                                                first_name = form.cleaned_data['first_name'],
                                                last_name = form.cleaned_data['last_name'],
                                                password='Brak')
            new_user.is_active = True
            new_user_manager = Manager.objects.create(user=new_user, email=form.cleaned_data['manager_email'])
            new_user_bonus = Bonus.objects.create(user=new_user, bonus=0)
            new_user_hash = Hash.objects.create(user=new_user, hash="nowy")
            new_user_bonus_days = BonusDays.objects.create(user=new_user, date_bonus="brak")
            new_user_pending = Pending.objects.create(user=new_user, dates='', status=0, url='')

            new_user.save()
            new_user_manager.save()
            new_user_bonus.save()
            new_user_hash.save()
            new_user_bonus_days.save()
            new_user_pending.save()
        return redirect('user')

    return render(request, 'user_view/add-user.html', {'form': form})

def reset_days(request):
    '''
    przed resetem wysłanie backup'a
    '''

    html_message = 'backup z dnia: ' + str(datetime.datetime.today())
    try:
        msg = EmailMultiAlternatives('Backup', html_message, 'no-reply', ['aaa@hlp.aq.pl'])
        msg.attach_alternative(html_message, "text/html")
        msg.attach_file('../db.sqlite3')
        msg.send()
        #send_mail('Backup', 'info', 'no-reply', ['aaa@hlp.aq.pl'], html_message=html_message)
    except:
        return HttpResponse('Nie wysłano kopii bazy danych, nie można przeprowadzić resetu dni.')
    td = datetime.datetime.today()
    for active_days in Bonus.objects.all():
        if active_days.bonus > 4.98:
            active_days.bonus = 4.98
            active_days.save()
    return HttpResponse('Dni zostały zresetowane do poziomu 3 miesięcy')

def backup(request):
    html_message = 'backup z dnia: ' + str(datetime.datetime.today())
    email = 'aaa@hlp.aq.pl'
    try:
        msg = EmailMultiAlternatives('Backup', html_message, 'no-reply', [email])
        msg.attach_alternative(html_message, "text/html")
        msg.attach_file('../db.sqlite3')
        msg.send()
        #send_mail('Backup', 'info', 'no-reply', ['aaa@hlp.aq.pl'], html_message=html_message)
    except:
        return HttpResponse('Nie wysłano kopii bazy danych, nie można przeprowadzić resetu dni.')
    return HttpResponse('backup został wysłany na email: ' + email )

def checkcallendar(request):
    if request.method == 'GET':
        form = PickUserForm()
    else:
        form = PickUserForm(request.POST)
        if form.is_valid():
            client_username = form.cleaned_data['client']
            client_username = str(client_username)
            wziete_db_q = BonusDays.objects.all().filter(user=User.objects.all().filter(username=client_username)).first().id
            client_id = str(wziete_db_q)
            cal = calendar.HTMLCalendar()
            cal = cal.formatyear(2017, width=4)
            td = datetime.datetime.today()  # dzisiejsza data, info dla js
            global swieta
            swieta = swieta
            form = PickUserForm(request.POST)
            wziete_db_q = BonusDays.objects.all().filter(user=User.objects.all().filter(pk=client_id))
            podzielone = wziete_db_q[0].date_bonus if wziete_db_q[0].date_bonus != "brak" else '0-1-2017'
            podzielone = podzielone.split(',')
            uzyte = json.dumps(podzielone)
            days_to_spend = Bonus.objects.all().filter(user=User.objects.all()
                                                       .filter(pk=client_id)).first().bonus
            nie_do_uzycia = swieta + podzielone
            nie_do_uzycia = json.dumps(nie_do_uzycia)#json.dumps(swieta + podzielone)
            return render(request, 'user_view/checkcallendar.html', {'form': form, 'warunek': True, 'cal':cal, 'td': td,
                                                                     'ndu': nie_do_uzycia, 'client_id': client_id,
                                                                     'daysused':uzyte,'used':podzielone})
        return HttpResponse('Walidacja nie została spełniona')
    return render(request, 'user_view/checkcallendar.html', {'form': form, 'warunek': False})


def force_bonus(request, user_id, days ):
    bonus = get_object_or_404(Bonus, user_id=user_id)
    if days >= 0:
        bonus.bonus = int(days)
        bonus.save()


